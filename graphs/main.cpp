#include "Simple_window.h"
#include "Graph.h"

#include <iostream>

double one(double) {
    return 1;
}

double square(double x) {
    return x*x;
}

double slope(double x) {
    return x/2;
}

int main()
{
    using namespace Graph_lib;

    const int xmax = 640;
    const int ymax = 480;

    const int x_orig = xmax/2;
    const int y_orig = ymax/2;
    const Point orig(x_orig, y_orig);

    const int r_min = -10;
    const int r_max = 11;

    const int n_points = 400;

    const int x_scale = 30;
    const int y_scale = 30;

    const int xlength = xmax - 40;
    const int ylength = ymax - 40;

    Simple_window win(Point(100, 100), xmax, ymax, "Graphs of functions");

    Axis x(Axis::x, Point(20, y_orig), xlength, xlength/x_scale, "one notch=1");
    x.set_color(Color::red);
    x.notches.set_color(Color::dark_red);
    x.label.move(-160, 0);
    x.label.set_font_size(12);
    win.attach(x);

    Axis y(Axis::y, Point(x_orig, ylength+20), ylength, ylength/y_scale, "one notch=1");
    y.set_color(Color::red);
    y.notches.set_color(Color::dark_red);
    y.label.set_font_size(12);
    win.attach(y);

    Function graph_one(one, r_min, r_max, orig, n_points, x_scale, y_scale);
    graph_one.set_color(Color::dark_blue);
    win.attach(graph_one);

    Function graph_square(square, r_min, r_max, orig, n_points, x_scale, y_scale);
    win.attach(graph_square);

    Function graph_slope(slope, r_min, r_max, orig, n_points, x_scale, y_scale);
    win.attach(graph_slope);

    Text ts_one(Point(x_orig-5*x_scale, y_orig-y_scale*int(one(5))), "1");
    ts_one.set_font_size(12);
    win.attach(ts_one);

    Text ts_square(Point(x_orig-2*x_scale, y_orig-y_scale*int(square(-2))), "x*x");
    ts_square.set_font_size(12);
    win.attach(ts_square);

    Text ts_slope(Point(x_orig-4*x_scale, y_orig-y_scale*int(slope(-4))), "x/2");
    ts_slope.set_font_size(12);
    win.attach(ts_slope);

    win.set_label("Graphics of three functions");
    win.wait_for_button();

    return 0;
}
