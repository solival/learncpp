#include<iostream>
#include<stdexcept>
#include<fstream>
#include<sstream>
#include<cmath>
#include<cstdlib>
#include<vector>

using namespace std;

const char number = '8';
const char quit = 'q';
const char print = ';';
const char name = 'a';
const char let = 'L';
const string declkey = "let";
const string resultprefix = "= ";
const string promptprefix = "> ";

class Variable {
public:
    string name;
    double value;
    Variable(string n, double v): name(n), value(v) {}
};

class Token {
    public:
        char kind;
        double value;
        string name;
        Token(char ch, double v): kind(ch),value(v) {}
        Token(char ch): kind(ch), value(0) {}
        Token(char ch, string n): kind(ch), name(n) {}
};

class Token_stream {
public:
    Token_stream(): full(false), buffer(0) {}
    Token get();      // get a Token (get() is defined elsewhere)
    void putback(Token t);    // put a Token back
    bool isdone();
    void ignore(char);
private:
    bool full;        // is there a Token in the buffer?
    Token buffer;     // here is where we keep a Token put back using putback()
};

// The putback() member function puts its argument back into the Token_stream's buffer:
void Token_stream::putback(Token t)
{
    if (full) throw runtime_error("putback() into a full buffer");
    buffer = t;       // copy t to buffer
    full = true;      // buffer is now full
}

Token Token_stream::get() {
    if (full) {       // do we already have a Token ready?
        // remove token from buffer
        full=false;
        return buffer;
    }

    char ch;
    cin >> ch;    // note that >> skips whitespace (space, newline, tab, etc.)

    switch (ch) {
    case print:    // for "print"
    case quit:    // for "quit"
    case '(': case ')': case '+': case '-': case '*': case '/':  case '%':
    case '{': case '}': case '!': case '^': case '=':
        return Token(ch);        // let each character represent itself
    case '.':
    case '0': case '1': case '2': case '3': case '4':
    case '5': case '6': case '7': case '8': case '9':
        {
            cin.putback(ch);         // put digit back into the input stream
            double val;
            cin >> val;              // read a floating-point number
            return Token(number,val);
        }
    default:
        if (isalpha(ch)) {
            string s;
            s += ch;
            while (cin.get(ch) && (isalpha(ch) || isdigit(ch))) s += ch;
            cin. putback(ch);
            if (s == declkey) return Token(let);
            return Token(name, s);
        }
        throw runtime_error("Bad token");
    }
}

/**
 * Check if token stream buffer is full at the moment
 */
bool Token_stream::isdone() {
    char input = cin.peek();
    if (input == 10) {
        return true;
    }
    return false;
}

void Token_stream::ignore(char c) {
    if (full && c == buffer.kind) {
        full = false;
        return;
    }
    full = false;

    char ch = 0;
    while(cin >> ch) {
        if (ch == c) {
            return;
        }
    }
}

Token_stream ts;
vector<Variable> var_table;

double get_value(string s) {
    for (int i = 0; i < var_table.size(); ++i) {
        if (var_table[i].name == s) {
            return var_table[i].value;
        }
    }
    throw runtime_error("undefined get variable");
}

void set_value(string s, double d) {
    for (int i = 0; i < var_table.size(); ++i) {
        if (var_table[i].name == s) {
            var_table[i].value = d;
            return;
        }
    }
    throw runtime_error("undefined set variable");
}

bool is_declared(string var) {
    for (int i = 0; i < var_table.size(); ++i) {
        if (var_table[i].name == var) return true;
    }
    return false;
}

double define_name(string var, double val) {
    if (is_declared(var)) throw runtime_error(var + " declared twice");
    var_table.push_back(Variable(var,val));
    return val;
}

double expression();

double declaration() {
    Token t = ts.get();
    if (t.kind != name) throw runtime_error("definition of variable requires name");
    string var_name = t.name;

    Token t2 = ts.get();
    if (t2.kind != '=') throw runtime_error("definition of variable missing = ");

    double d = expression();
    define_name(var_name, d);
    return d;
}

double statement() {
    Token t = ts.get();
    switch (t.kind) {
    case let:
        return declaration();
    default:
        ts.putback(t);
        return expression();
    }
}

double primary() {
    Token t = ts.get();
    switch (t.kind) {
    case '(': {
        double d = expression();
        t = ts.get();
        if (t.kind != ')') throw runtime_error("')' expected");
        return d;
    }
    case '{': {
        double d = expression();
        t = ts.get();
        if (t.kind != '}') throw runtime_error("'}' expected");
        return d;
    }
    case number:
        return t.value;
    case name:
        return get_value(t.name);
    case '-':
        return - primary();
    case '+':
        return primary();
    default:
        throw runtime_error("Expression not found");
    }
}

double fact() {
    double left = primary();

    Token t = ts.get();
    while (true) {
        switch (t.kind) {
        case '!':
            {
                double d = 1;
                while (left > 1) {
                    d *= left;
                    left--;
                }
                left = d;
                break;
            }
        case '^':
            left = pow(left, primary());
            break;
        default:
            ts.putback(t);
            return left;
        }
        t = ts.get();
    }
}

double term() {
    double left = fact();
    Token t = ts.get();
    while (true) {
        switch (t.kind) {
        case '*':
            left *= fact();
            break;
        case '/':
            {
                double d = fact();
                if (d == 0) {
                    throw runtime_error("div/0");
                }
                left /= d;
                break;
            }
        case '%':
            {
                double d = fact();
                int i1 = int(left);
                int i2 = int(d);
                if (i1 != left || i2 != d) {
                    throw runtime_error("cannot take modulus for non integer numbers");
                }
                if (i2 == 0) {
                    throw runtime_error("div/0");
                }
                left = i1%i2;
            }
            break;
        default:
            ts.putback(t);
            return left;
        }
        t = ts.get();
    }
}

double expression() {
    double left = term();
    Token t = ts.get();
    while (true) {
        switch (t.kind) {
        case '+':
            left += term();
            break;
        case '-':
            left -= term();
            break;
        default:
            ts.putback(t);
            return left;
        }
        t = ts.get();
    }
}

void calculate() {
    cout << promptprefix;
    while (cin) {
        try {
            Token t = ts.get();
            if (ts.isdone()) {
                cout << promptprefix;
            }

            while (t.kind == print) t=ts.get();
            if (t.kind == quit) {
                return;
            }

            ts.putback(t);
            cout << resultprefix << statement() << endl;
        }
        catch (exception &e) {
            cerr << e.what() << endl;
            ts.ignore(print);
        }
    }
}
int main()
{
    define_name("pi", 3.1415926535);
    define_name("e", 2.7182817284);
    calculate();
    return 0;
}
