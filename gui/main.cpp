#include <iostream>
#include "GUI.h"
#include "Graph.h"
#include "Window.h"

using namespace std;
using namespace Graph_lib;

struct Lines_window : Graph_lib::Window {
    Lines_window(Point xy, int w, int h, const string& title);
    Open_polyline lines;

private:
    Button next_button;
    Button quit_button;
    Button menu_button;
    In_box next_x;
    In_box next_y;
    Out_box xy_out;
    Menu color_menu;

    static void cb_next(void*, void* pw) { reference_to<Lines_window>(pw).next(); }
    void next();

    static void cb_quit(void*, void* pw) { reference_to<Lines_window>(pw).quit(); }
    void quit() { hide(); }

    static void cb_menu(void*, void* pw) { reference_to<Lines_window>(pw).menu_pressed(); }
    void menu_pressed() { menu_button.hide(); color_menu.show(); }
    void hide_menu() { color_menu.hide(); menu_button.show(); }

    static void cb_red(void*, void* pw) { reference_to<Lines_window>(pw).red_pressed(); }
    void red_pressed() { change(Color::red); hide_menu(); }
    static void cb_blue(void*, void* pw) { reference_to<Lines_window>(pw).blue_pressed(); }
    void blue_pressed() { change(Color::blue); hide_menu(); }
    static void cb_black(void*, void* pw) { reference_to<Lines_window>(pw).black_pressed(); }
    void black_pressed() { change(Color::black); hide_menu(); }

    void change(Color c) { lines.set_color(c); redraw(); }
};

Lines_window::Lines_window(Point xy, int w, int h, const string& title):
    Window(xy, w, h, title),
    next_button(Point(x_max()-150, 0), 70, 20, "Next point", cb_next),
    quit_button(Point(x_max()-70, 0), 70, 20, "Quit", cb_quit),
    menu_button(Point(x_max()-80, 30), 80, 20, "Color menu", cb_menu),
    next_x(Point(x_max()-310, 0), 50, 20, "next x:"),
    next_y(Point(x_max()-210, 0), 50, 20, "next y:"),
    xy_out(Point(100, 0), 100, 20, "current (x,y):"),
    color_menu(Point(x_max()-70, 30), 70, 20, Menu::vertical, "color") {

    attach(next_button);
    attach(quit_button);
    attach(next_x);
    attach(next_y);
    attach(xy_out);
    attach(lines);
    attach(menu_button);

    color_menu.attach(new Button(Point(0, 0), 0, 0, "red", cb_red));
    color_menu.attach(new Button(Point(0, 0), 0, 0, "blue", cb_blue));
    color_menu.attach(new Button(Point(0, 0), 0, 0, "black", cb_black));
    attach(color_menu);
    color_menu.hide();

    lines.add(Point(200, 200));
    lines.add(Point(400, 200));
    lines.add(Point(400, 400));
    lines.add(Point(200, 400));
    lines.add(Point(200, 200));
}

void Lines_window::next() {
    int x = next_x.get_int();
    int y = next_y.get_int();

    lines.add(Point(x,y));

    stringstream ss;
    ss << '(' << x << ',' << y << ')';
    xy_out.put(ss.str());
    redraw();
}

int main()
{
    try {
        Lines_window main_window(Point(100,200), 640, 480, "Testing GUI");

        return Fl::run();
    }
    catch (exception &e) {
        cerr << "Exception: " << e.what() << '\n';
        return 1;
    }
}
