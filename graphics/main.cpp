#include "Simple_window.h"
#include "Graph.h"

#include <iostream>

int main()
{
    using namespace Graph_lib;

    Point t1(100,100);
    Simple_window win(t1, 600, 400, "test");

    Polygon poly;

    poly.add(Point(300,200));
    poly.add(Point(350,100));
    poly.add(Point(400,200));
    poly.set_color(Color::red);
    poly.set_style(Line_style(Line_style::dash, 4));
    win.attach(poly);

    Axis xa(Axis::x, Point(20, 300), 280, 10, "Axis X");
    win.attach(xa);

    Axis ya(Axis::y, Point(20, 300), 280, 10, "Axis Y");
    ya.set_color(Color::cyan);
    ya.label.set_color(Color::dark_red);
    win.attach(ya);

    win.set_label("Draw garph");

    Function sine(sin, 0, 100, Point(20, 150), 1000, 50, 50);
    win.attach(sine);

    Rectangle r(Point(200, 200), 100, 50);
    r.set_fill_color(Color::yellow);
    win.attach(r);

    Open_polyline poly_rect;
    poly_rect.add(Point(100, 50));
    poly_rect.add(Point(200, 50));
    poly_rect.add(Point(200, 100));
    poly_rect.add(Point(100, 100));
    poly_rect.add(Point(50, 75));
    poly_rect.add(Point(100, 50));
    poly_rect.set_style(Line_style(Line_style::dash, 2));
    poly_rect.set_fill_color(Color::green);
    win.attach(poly_rect);

    Text t(Point(150,150), "Hello Graph World!!!");
    t.set_font(Graph_lib::Font::times_bold);
    t.set_font_size(20);
    win.attach(t);

    Image i1(Point(0, 0), "image.jpg");
    win.attach(i1);
    i1.move(470, 240);

    Mark m(Point(100, 200), 'x');
    win.attach(m);

    Image cal(Point(225, 225), "anim.gif");
    cal.set_mask(Point(40,40), 200, 150);
    win.attach(cal);

    win.wait_for_button();

    return 0;
}
