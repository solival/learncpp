#include <iostream>
#include <istream>
#include <ostream>
#include <vector>
#include <stdexcept>
#include <fstream>
#include <sstream>
#include <cstdlib>

using namespace std;

struct Reading {
    int hour;
    double temperature;
    Reading(int h, double t) :hour(h), temperature(t) {}
};

int main()
{
    cout << "Enter input filename: ";
    string name;
    cin >> name;
    ifstream ist(name.c_str());
    if (!ist) throw runtime_error("Cannot open file " + name);

    cout << "Enter output filename: ";
    cin >> name;
    ofstream ost(name.c_str());
    if (!ost) throw runtime_error("Cannot open file " + name);

    vector<Reading> temps;
    int hour;
    double temperature;

    while (ist >> hour >> temperature) {
        if (hour < 0 || 23 < hour) throw runtime_error("Wrong time: " + hour);
        temps.push_back(Reading(hour, temperature));
    }

    for (int i = 0; i < temps.size(); ++i) {
        ost << '(' << temps[i].hour << ',' << temps[i].temperature << ")\n";
    }

    return 0;
}
